
public class main {	
	public static String rotate(String s, int k){
		if(s.length() == 0)
			return s;
		int shift = k % s.length();
		System.out.print(s + " " + k + " ");
		if(shift<0){
			System.out.print(s.substring((shift*-1),s.length()) + s.substring(0,(shift*-1)) + "\n");
			return s.substring((shift*-1),s.length()) + s.substring(0,(shift*-1));
		}
		else{
			System.out.print(s.substring(s.length()-shift,s.length()) + s.substring(0,s.length()-shift) + "\n");
			return s.substring(s.length()-shift,s.length()) + s.substring(0,s.length()-shift);
		}
	}
}
