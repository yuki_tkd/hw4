import static org.junit.Assert.*;
import org.junit.Test;


public class maintest {
	@Test
	public void rotate0() {
		assertEquals(main.rotate("abcde",0), "abcde");
	}
	@Test
	public void rotater1() {
		assertEquals(main.rotate("abcde",1), "eabcd");
	}
	@Test
	public void rotatel1() {
		assertEquals(main.rotate("abcde",-1), "bcdea");
	}
	
	@Test
	public void rotater5() {
		assertEquals(main.rotate("abcde",5), "abcde");
	}
	
	@Test
	public void rotatel5() {
		assertEquals(main.rotate("abcde",-5), "abcde");
	}
	
	@Test
	public void rotater6() {
		assertEquals(main.rotate("abcde",6), "eabcd");
	}
	@Test
	public void rotatel6() {
		assertEquals(main.rotate("abcde",-6), "bcdea");
	}
	@Test
	public void rotates0() {
		assertEquals(main.rotate("",0), "");
	}
}
